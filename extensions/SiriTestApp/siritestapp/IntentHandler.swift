//
//  IntentHandler.swift
//  siritestapp
//
//  Created by Adnan on 21/01/17.
//
//

import Intents

// As an example, this class is set up to handle Message intents.
// You will want to replace this or add other intents as appropriate.
// The intents you wish to handle must be declared in the extension's Info.plist.

// You can test your example integration by saying things to Siri like:
// "Send a message using Siritest"
// "Siritest John saying hello"

// INSendMessageIntentHandling - This intent (class) handles the sending of messages to 
// the designated user/users.

class IntentHandler: INExtension, INSendMessageIntentHandling, INSendPaymentIntentHandling {
    
    enum SupportedCurrencies : String{
        case USD
        case INR
    }
   
    static func allValues() -> [String]{
        let allValues: [SupportedCurrencies] = [.USD, .INR]
        return allValues.map({$0.rawValue})// return string
    }
    
    static var defaultCurrency = SupportedCurrencies.INR
    
    override func handler(for intent: INIntent) -> Any {
        // This is the default implementation.  If you want different objects to handle different intents,
        // you can override this and return the handler you want for that particular intent.
        
        return self
    }
    
    
    //RESOLVING Recipient for SEND MESSAGE  INTENT
    func resolveRecipients(forSendMessage intent: INSendMessageIntent, with completion: @escaping ([INPersonResolutionResult]) -> Void) {
        if let recipients = intent.recipients {
            
            // If no recipients were provided we'll need to prompt for a value.
            if recipients.count == 0 {
                completion([INPersonResolutionResult.needsValue()])
                return
            }
            
            var resolutionResults = [INPersonResolutionResult]()
            for recipient in recipients {
                let matchingContacts = [recipient] // Implement your contact matching logic here to create an array of matching contacts
                switch matchingContacts.count {
                case 2  ... Int.max:
                    // We need Siri's help to ask user to pick one from the matches.
                    resolutionResults += [INPersonResolutionResult.disambiguation(with: matchingContacts)]
                    
                case 1:
                    // We have exactly one matching contact
                    resolutionResults += [INPersonResolutionResult.success(with: recipient)]
                    
                case 0:
                    // We have no contacts matching the description provided
                    resolutionResults += [INPersonResolutionResult.unsupported()]
                    
                default:
                    break
                    
                }
            }
            completion(resolutionResults)
        }
    }
    
    //RESOLVING content for SEND MESSAGE INTENT
    // Validate the information recieved else prompt Siri to ask further question
    func resolveContent(forSendMessage intent: INSendMessageIntent, with completion: @escaping (INStringResolutionResult) -> Void) {
        if let text = intent.content, !text.isEmpty {
            completion(INStringResolutionResult.success(with: text))
        } else {
            completion(INStringResolutionResult.needsValue())
        }
    }
    
    // Once resolution is completed and provide CONFIRMATION
    
    func confirm(sendMessage intent: INSendMessageIntent, completion: @escaping (INSendMessageIntentResponse) -> Void) {
        // Verify user is authenticated and your app is ready to send a message.
        
        let userActivity = NSUserActivity(activityType: NSStringFromClass(INSendMessageIntent.self))
        let response = INSendMessageIntentResponse(code: .ready, userActivity: userActivity)
        completion(response)
    }
    
    // Handle MESSAGE INTENTS
    // INSendMessageIntent - This intent (class) requests the sending of a message to the designated recipients.
    // INSendMessageIntentResponse - This intent contains the app’s response to a send message intent.
    
    func handle(sendMessage intent: INSendMessageIntent, completion: @escaping (INSendMessageIntentResponse) -> Void) {
        // Implement your application logic to send a message here.
        
        let userActivity = NSUserActivity(activityType: NSStringFromClass(INSendMessageIntent.self))
        let response = INSendMessageIntentResponse(code: .success, userActivity: userActivity)
        completion(response)
    }
    
    
    //RESOLVING PAYMENT INTENT
    // Resolve the currency amount to be transferred.
    func resolveCurrencyAmount(forSendPayment intent: INSendPaymentIntent, with completion: @escaping (INCurrencyAmountResolutionResult) -> Void) {
        
        // Resolve the currency amount.
            if let currencyAmount = intent.currencyAmount, let amount = currencyAmount.amount{
                if amount.intValue <= 0 {
                    // The amount needs to be a positive value.
                    completion(INCurrencyAmountResolutionResult.unsupported())
                }else{
                    completion(INCurrencyAmountResolutionResult.success(with: currencyAmount ))
                }
            }        
    }
    
    // HANDLE PAYMENT INTENTS
    // INSendPaymentIntent - This intent (class) requests the transfer of money from the current user’s account to a different user’s account.
    // INSendPaymentIntentResponse - This intent contains the app’s response to a send payment intent.
    
    func handle(sendPayment intent: INSendPaymentIntent, completion: @escaping (INSendPaymentIntentResponse) -> Void) {
        // Check that we have valid values for payee and currencyAmount
        guard let payee = intent.payee, let amount = intent.currencyAmount else {
            return completion(INSendPaymentIntentResponse(code: .unspecified, userActivity: nil))
        }
        // Make your payment!
        print("Sending \(amount) payment to \(payee)!")
        completion(INSendPaymentIntentResponse(code: .success, userActivity: nil))
    }
    

}

