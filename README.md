# Sirikit #

Sirikit is used to support Siri giving users the ability to control your app’s behavior using their voice.  Of the various domains that Siri supports we are going to demonstrate using our app for the follwing two domains:

* Messaging - Sending a message to someone using the app
* Payments - Send a payment to someone using the App

These domains define one of more tasks that an app can perform. These tasks are known as **Intents** because they represent the intentions of the user. Intents are the basis of your app’s communication with SiriKit.

When the user makes a request through Siri, the system fills an intent object with the details of the request and delivers that object to your app extension. You use the intent object to validate the request data and perform the associated task.

Thus, the *goal of our app* is to process requests through Siri such as 

* *'Send a **message** via SiritestApp app'*
* *'Send <**$amount**> to <person> using SiritestApp app'*

![IMG_2964.PNG](https://bitbucket.org/repo/xKaaoj/images/2007135403-IMG_2964.PNG)

![IMG_2967.jpg](https://bitbucket.org/repo/xKaaoj/images/3253883379-IMG_2967.jpg)

### What is this repository for? ###

A titanium app that has an app extension inside its app bundle (*extensions directory*). The app was created using Appcelerator Studio and the extension using Xcode. 

### Architecture ###
* A SiriKit extension is composed of two types of extension. An **Intents extension** is required and takes care of handling the requests by the user and executing a specific task in your app (such as starting a call, sending a message, etc.).
* On the other hand, an **IntentsUI extension** is not mandatory. You should only create one if you want to customize the user interface that Siri shows when presenting your data. 

### How does it work? ###
* *How Siri Handles request* - Siri manages users request by turning them into intents object for us to handle. Siri handles all of the natural language processing required to turn spoken requests into valid data in an intent object. This intent object can be used for the validation process and we can programatically ask Siri to do further prompting.

* *Extension mapping to Titanium App* - The native extension was created and then copied inside the Titanium app root directory named *extensions*
Inside the *tiapp.xml* ios section the extensions linking can be found. The provisioning profile id for the extension app is added here as well.

*References:* [Creating Intents extension](https://developer.apple.com/library/content/documentation/Intents/Conceptual/SiriIntegrationGuide/CreatingtheIntentsExtension.html#//apple_ref/doc/uid/TP40016875-CH4-SW1)

* *Resolving and Handling Intents (IntentHandler.swift)* - The entry point of the Intents extension is the *INExtension* object, whose sole job is to direct Siri to the objects capable of handling incoming intents. In the extension, there are three types of objects that are used to handle intents:

* An intent object contains the data that Siri gathered from the user.
* A handler object is a custom object that you use to resolve, confirm, and handle the intent.
* A response object contains your response to the intent.

When SiriKit has an intent object for your app to handle, it asks your INExtension object to provide a handler object capable of handling the intent.Handler objects adopt specific protocols for the intents they handle. The handler protocols are all structured identically, with methods for resolving, confirming, and handling the specified intent.

Likewise, for handling the *Send message* we have added an handler as

```
#!swift

    func handle(sendMessage intent: INSendMessageIntent, completion: @escaping (INSendMessageIntentResponse) -> Void) {        
        let userActivity = NSUserActivity(activityType: NSStringFromClass(INSendMessageIntent.self))
        let response = INSendMessageIntentResponse(code: .success, userActivity: userActivity)
        completion(response)
    }

```
For handling the Send Payment 

```
#!swift

    func handle(sendPayment intent: INSendPaymentIntent, completion: @escaping (INSendPaymentIntentResponse) -> Void) {
        guard let payee = intent.payee, let amount = intent.currencyAmount else {
            return completion(INSendPaymentIntentResponse(code: .unspecified, userActivity: nil))
        }
        print("Sending \(amount) payment to \(payee)!")
        completion(INSendPaymentIntentResponse(code: .success, userActivity: nil))
    }

```

Because requests coming from Siri involve spoken commands, the initial information provided by the user may be incomplete or ambiguous. We can look at each parameter and let Siri know if the provided value is satisfactory or if we need Siri to ask further questions of the user.
We used the following methods to do this:

*For Send Message Intent*
```
#!swift
resolveRecipients
resolveContent
confirm
```
*For Send Payment Intent*
```
#!swift
resolveCurrencyAmount
```
For a list of classes and protocols used to implement intent, response, and handler objects, see [Intents Domains. ](https://developer.apple.com/library/content/documentation/Intents/Conceptual/SiriIntegrationGuide/SiriDomains.html#//apple_ref/doc/uid/TP40016875-CH9-SW2)


### How do I get set up? ###

* Ti SDK 6.0
* XCode 8+
* Provisioning certificates for the main app (device) and the target app (the app extension) with Sirikit capability enabled. The simulators do not support Siri.
* Enable the Siri -> App Support inside the Settings of your device

### Who do I talk to? ###

* Adnan F. Baliwala

### References ###
* [SiriKit Programming Guide](https://developer.apple.com/library/prerelease/content/documentation/Intents/Conceptual/SiriIntegrationGuide/index.html#//apple_ref/doc/uid/TP40016875-CH11-SW1) - This is the the best url to understand the Sirikit concept. 

* [Creating Siri Intent and linking with Titanium](http://docs.appcelerator.com/platform/latest/#!/guide/Creating_iOS_Extensions_-_Siri_Intents) - Integration simplified!

* [General App Extension Programming Guide](https://developer.apple.com/library/content/documentation/General/Conceptual/ExtensibilityPG/index.html#//apple_ref/doc/uid/TP40014214) - You can find other more interesting extensions here.

* [Helpful for Resolving/Validating currency parameter](https://developer.apple.com/library/content/samplecode/IntentHandling/Listings/Projects_Payments_PaymentsIntentsExtension_SendPaymentIntentHandler_swift.html) - Helpful for validation when the user data is gathered by Siri.

* [Payment Intent implementation](https://samsymons.com/blog/creating-siri-extensions-with-sirikit/) - Provides insight into how to handle payment category/domain intent. 

* [Building Xcode App and its extension both in XCode](https://swiftyos.wordpress.com/2016/09/24/radical-message-learn-to-add-sirikit-in-your-messages-app/) - Helpful for Sending message intent.